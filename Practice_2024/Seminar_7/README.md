## Overview

## TODO: меньше текста, больше примеров

## TODO: Процедуры, Триггеры - расписать получше (?)

> ### Примечание:
> При желании разобраться более подробно - рекомендую к прочтению официальную документацию:<br>
> [postgresql - на русском](https://postgrespro.ru/docs/postgresql/current/)<br>
> [postgresql - docs (en)](https://www.postgresql.org/docs/current/)

## Материалы к семинарам:

### - [Функции. Процедуры. Триггеры.](TRIGGERS.md)

### - [CTE, VIEW](CTE_VIEWS.md)

### - [Индексы](INDEXES.md)
